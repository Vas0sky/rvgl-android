# RVGL Android Project

Part of the [RVGL Base Repository](https://gitlab.com/re-volt/rvgl-base). 
Do not clone this repository directly. The base repository includes this 
as a sub-module.

## Building

To build, you need:

- Android SDK
- NDK Toolchain
- Apache Ant

Clone `rvgl-base` using `git clone --recursive` to get needed sub-modules.

Run: `./build.sh --prebuilt`.

