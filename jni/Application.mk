
# Uncomment this if you're using STL in your project
# See CPLUSPLUS-SUPPORT.html in the NDK documentation for more information
APP_STL := gnustl_static

ifdef DEBUG
APP_ABI := armeabi-v7a
else
APP_ABI := armeabi-v7a arm64-v8a x86 x86_64
endif

APP_LDFLAGS := -nodefaultlibs

APP_PLATFORM := android-12

NDK_TOOLCHAIN_VERSION := 4.9
