#!/bin/bash

for i in "$@"; do
  case $i in
    --full) full=true;;
  esac
done

(cd ../distrib && ./init.sh)

rm -rf assets
mkdir -p assets

if [ "$full" = true ]; then
  cp -asf $(realpath ../distrib/game_files)/* ./assets
  cp -asf $(realpath ../distrib/dcpack)/* ./assets
  cp -asf $(realpath ../distrib/ost)/* ./assets
fi

cp -asf $(realpath ../distrib/assets)/* ./assets
cd assets

re=^.*assets_list.*$\|^\./$
find . -depth -print0 | while IFS= read -r -d '' file; do
  dest="${file#*./}"
  if [ -d "$file" ]; then
    dest="$dest/"
  fi
  if [[ ! -z "$dest" && ! "$dest" =~ $re ]]; then
    echo "$dest"
  fi
done | sort > assets_list.txt
